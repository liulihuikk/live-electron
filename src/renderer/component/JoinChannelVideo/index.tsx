import {
  ClientRoleType,
  LocalVideoStreamError,
  LocalVideoStreamState,
  VideoSourceType,
} from 'agora-electron-sdk';

import { useCallback, useEffect, useState } from 'react';
import { Row, Col, Button } from 'antd';
import * as log from '../../utils/log';
import useInitRtcEngine from '../../hooks/useInitRtcEngine';
import css from './index.module.scss';
import BaseRenderUsers from '../BaseRenderUsers';

const JoinChannelVideo = () => {
  const [enableVideo] = useState<boolean>(true);
  const {
    channelId,
    token,
    uid,
    joinChannelSuccess,
    remoteUsers,
    startPreview,
    engine,
  } =
    /**
     * Step 1: initRtcEngine
     */
    useInitRtcEngine(enableVideo);

  /**
   * Step 2: joinChannel
   */
  const joinChannel = () => {
    if (!channelId) {
      log.error('channelId is invalid');
      return;
    }
    if (uid < 0) {
      log.error('uid is invalid');
      return;
    }

    // start joining channel
    // 1. Users can only see each other after they join the
    // same channel successfully using the same app id.
    // 2. If app certificate is turned on at dashboard, token is needed
    // when joining channel. The channel name and uid used to calculate
    // the token has to match the ones used for channel join
    engine.current.joinChannel(token, channelId, uid, {
      // Make myself as the broadcaster to send stream to remote
      clientRoleType: ClientRoleType.ClientRoleBroadcaster,
    });
  };

  /**
   * Step 3: leaveChannel
   */
  const leaveChannel = () => {
    engine.current.leaveChannel();
  };

  const onVideoDeviceStateChanged = useCallback(
    (deviceId: string, deviceType: number, deviceState: number) => {
      log.info(
        'onVideoDeviceStateChanged',
        'deviceId',
        deviceId,
        'deviceType',
        deviceType,
        'deviceState',
        deviceState
      );
    },
    []
  );

  const onLocalVideoStateChanged = useCallback(
    (
      source: VideoSourceType,
      state: LocalVideoStreamState,
      error: LocalVideoStreamError
    ) => {
      log.info(
        'onLocalVideoStateChanged',
        'source',
        source,
        'state',
        state,
        'error',
        error
      );
    },
    []
  );

  useEffect(() => {
    engine.current.addListener(
      'onVideoDeviceStateChanged',
      onVideoDeviceStateChanged
    );
    engine.current.addListener(
      'onLocalVideoStateChanged',
      onLocalVideoStateChanged
    );

    const engineCopy = engine.current;
    return () => {
      engineCopy.removeListener(
        'onVideoDeviceStateChanged',
        onVideoDeviceStateChanged
      );
      engineCopy.removeListener(
        'onLocalVideoStateChanged',
        onLocalVideoStateChanged
      );
    };
  }, [engine, onLocalVideoStateChanged, onVideoDeviceStateChanged]);

  return (
    <div className={css.container}>
      <h1>直播组件</h1>
      <Button
        onClick={joinChannelSuccess ? leaveChannel : joinChannel}
        type="primary"
      >
        {joinChannelSuccess ? '下课' : '上课'}
      </Button>
      <Row>
        <Col span={6}>
          <BaseRenderUsers
            enableVideo={enableVideo}
            startPreview={startPreview}
            joinChannelSuccess={joinChannelSuccess}
            remoteUsers={remoteUsers}
          />
        </Col>
      </Row>
    </div>
  );
};

export default JoinChannelVideo;
