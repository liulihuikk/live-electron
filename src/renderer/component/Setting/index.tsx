import { Button, Form, Input } from 'antd';
import { useNavigate } from 'react-router-dom';
import config from 'renderer/config/agora.config';

type FieldType = {
  appId?: string;
  channelId?: string;
  token?: string;
  uid?: string;
};

const Setting = () => {
  const navigate = useNavigate();

  const onFinish = (values: any) => {
    console.log('Success:', values);
    config.appId = values.appId;
    config.channelId = values.channelId;
    config.token = values.token;
    config.uid = Number(values.uid);
    navigate('/live');
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      style={{ maxWidth: 600 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item<FieldType>
        label="AppID"
        name="appId"
        rules={[{ required: true, message: 'Please input your AppID!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType>
        label="ChannelId"
        name="channelId"
        rules={[{ required: true, message: 'Please input your channelId!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType>
        label="Token"
        name="token"
        rules={[{ required: true, message: 'Please input your token!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType> label="Uid" name="uid">
        <Input placeholder="Please input uid" />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          进入直播
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Setting;
