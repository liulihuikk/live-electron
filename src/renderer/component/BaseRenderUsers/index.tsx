/* eslint-disable react/require-default-props */
import { VideoCanvas, VideoSourceType } from 'agora-electron-sdk';
import React, { ReactElement, memo } from 'react';
import { Card } from 'antd';

import { RtcSurfaceView } from '../RtcSurfaceView';

import { AgoraList } from '../ui';

export interface BaseRenderUsersProps {
  enableVideo: boolean;
  startPreview?: boolean;
  joinChannelSuccess: boolean;
  remoteUsers: number[];
  renderUser?: (_user: VideoCanvas) => ReactElement | undefined;
  renderVideo?: (_user: VideoCanvas) => ReactElement | undefined;
}

const BaseRenderUsers = ({
  enableVideo,
  startPreview,
  joinChannelSuccess,
  remoteUsers,
  renderVideo = (user) => <RtcSurfaceView canvas={user} />,
  renderUser = (user) => {
    return (
      <Card title={`${user.uid} - ${user.sourceType}`}>
        {enableVideo ? (
          <>
            <div>Click view to mirror</div>
            {renderVideo(user)}
          </>
        ) : undefined}
      </Card>
    );
  },
}: BaseRenderUsersProps) => {
  return (
    <>
      {!!startPreview || joinChannelSuccess
        ? renderUser({
            uid: 0,
            sourceType: VideoSourceType.VideoSourceCamera,
          })
        : undefined}
      {!!startPreview || joinChannelSuccess ? (
        <AgoraList
          data={remoteUsers}
          renderItem={(item) =>
            renderUser({
              uid: item,
              sourceType: VideoSourceType.VideoSourceRemote,
            })
          }
        />
      ) : undefined}
    </>
  );
};

export default memo(BaseRenderUsers);
