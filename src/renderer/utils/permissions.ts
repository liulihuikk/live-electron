const { ipcRenderer } = window.require('electron');

export type mediaType = 'microphone' | 'camera' | 'screen';
export interface AskMediaAccessReturn {
  result: boolean;
  mediaType: mediaType;
}

/**
 * request media permission MACOS ONLY
 * If an access request was denied and later is changed through the System Preferences pane, a restart of the app will be required for the new permissions to take effect.
 * If access has already been requested and denied, it must be changed through the preference pane;
 * this fun will not call and the promise will resolve with the existing access status.
 * @param mediaTypes
 * @returns AskMediaAccessReturn[]
 */
export const askMediaAccess = async (
  mediaTypes: mediaType[]
): Promise<AskMediaAccessReturn[]> => {
  const results: AskMediaAccessReturn[] = [];
  if (process.platform === 'darwin') {
    await Promise.all(
      mediaTypes.map(async (mediaTypeItem) => {
        let result: boolean = false;
        try {
          result = await ipcRenderer.invoke('IPC_REQUEST_PERMISSION_HANDLER', {
            type: mediaTypeItem,
          });
        } catch (error) {
          result = error as boolean;
        } finally {
          results.push({
            mediaType: mediaTypeItem,
            result,
          });
        }
      })
    );
  }
  console.log(results);
  return results;
};
