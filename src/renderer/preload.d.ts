/* eslint-disable no-unused-vars */
import type { IRtcEngine } from 'agora-electron-sdk';

declare global {
  interface Window {
    agoraRtcEngine?: IRtcEngine;
  }
}

export {};
