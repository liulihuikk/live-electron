import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import { createAgoraRtcEngine } from 'agora-electron-sdk';
import './App.css';
import { useEffect } from 'react';
import LiveVideo from './feature/LiveVideo';
import Setting from './component/Setting';
import LiveVideoShare from './feature/LiveVideoShare';
import * as log from './utils/log';

export default function App() {
  useEffect(() => {
    const engine = createAgoraRtcEngine();
    log.log(engine.getVersion());
  }, []);

  return (
    <Router>
      <Routes>
        {/* <Route path="/" element={<Setting />} /> */}
        <Route path="/" element={<LiveVideoShare />} />
        <Route path="/live" element={<LiveVideo />} />
      </Routes>
    </Router>
  );
}
