import { createRoot } from 'react-dom/client';
import App from './App';

const { ipcRenderer } = window.require('electron');

const container = document.getElementById('root')!;
const root = createRoot(container);
root.render(<App />);

ipcRenderer.send('ipc-example', ['ping']);
