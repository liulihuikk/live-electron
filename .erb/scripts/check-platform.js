// check system to set the agora_electron in root package.json
const fs = require('fs');
const path = require('path');

let config;
if (process.platform === 'darwin') {
  config = {
    platform: 'darwin',
    prebuilt: true,
  };
} else {
  config = {
    platform: 'win32',
    prebuilt: true,
    arch: 'ia32',
  };
}

const packageJsonPath = path.join(__dirname, '../../package.json');
const packageJson = JSON.parse(fs.readFileSync(packageJsonPath, 'utf8'));
packageJson.agora_electron = config;
fs.writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 2));
